from django.apps import AppConfig


class WonderwebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WonderWeb'
