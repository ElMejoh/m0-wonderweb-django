from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from WonderWeb.models import Preguntas


def index(request):
     preguntas = Preguntas.objects.all()
     respuesta = ''

     for pregunta in preguntas:
         respuesta += pregunta.pregunta + '<br>'

     return render(request, "WonderWeb/Tutorial/index.html", {
         "preguntas" : preguntas
     })

def details(request, pregunta_id):
    pregunta = Preguntas.objects.get(pk=pregunta_id)
    return render(request, "WonderWeb/Tutorial/details.html", {
        "pregunta" : pregunta
    })

def home(request):
    return render(request, "WonderWeb/home_HTML.html")

def contactanos(request):
    return render(request, "WonderWeb/contactanos_HTML.html")

def entornoVirtual(request):
    return render(request, "WonderWeb/entornoVirtual_HTML.html")

def registro(request):
    return render(request, "WonderWeb/registro_HTML.html")

def proyectos(request):
    return render(request, "WonderWeb/proyectos_HTML.html")


def result(request, pregunta_id):
    return HttpResponse('Estás mirando las respuestas de la pregunta %s.' % pregunta_id)

def vote(request, pregunta_id):
    return HttpResponse('Estás mirando los votos de la pregunta %s.' % pregunta_id)


# preguntas = Preguntas.objects.all()
#     respuesta = ''
#
#     for pregunta in preguntas:
#         respuesta += pregunta.pregunta + '<br>'