from django.db import models

class Preguntas(models.Model):
    pregunta = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.pregunta


class Respuestas(models.Model):
    preguntaFK = models.ForeignKey(Preguntas, on_delete=models.CASCADE)
    respuesta = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    def __str__(self):
        return self.respuesta