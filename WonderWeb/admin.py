from django.contrib import admin

from WonderWeb.models import Preguntas, Respuestas

admin.site.register(Preguntas)
admin.site.register(Respuestas)
