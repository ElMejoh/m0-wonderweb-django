from django.urls import path
from . import views

urlpatterns = [
    # Index
    path('', views.index, name='index'),

    # Detalles de una pregunta
    path('<int:pregunta_id>/', views.details, name='details'),

    # Las respuestas a una pregunta
    path('<int:pregunta_id>/results/', views.result, name='result'),

    # Los votos a una respuesta
    path('<int:pregunta_id>/vote/', views.vote, name='vote'),





    # HOME
    path('home/', views.home, name='home'),

    # EntornoVirtual
    path('entornoVirtual/', views.entornoVirtual, name='entornoVirtual'),

    # Contactanos
    path('contactanos/', views.contactanos, name='contactanos'),

    # Registro de Ususario
    path('registro/', views.registro, name='registro'),

    # Proyectos
    path('proyectos/', views.proyectos, name='proyectos'),

]